import configparser
import datetime
import json
import logging
import sys
import traceback

import requests


# Config
config = configparser.ConfigParser()
config.read('config.ini')


# Uncaught exception handling
def uncought_exception_handler(type, value, tb):
    logging.error(json.dumps({'time': str(datetime.datetime.now()),
                              'status': 'exception',
                              'traceback': traceback.format_tb(tb)}))
    exit(-1)


sys.excepthook = uncought_exception_handler

# Logger
logging.basicConfig(filename='ddclient.log', filemode='a', format='%(message)s', level=logging.WARNING)

# Cloudflare
cf_base_url = 'https://api.cloudflare.com/client/v4/'
zone_name = config['cloudflare']['zone_name']
dns_record_name = config['cloudflare']['dns_record_name']

# Cloudflare headers
content_type = 'application/json'
email = config['cloudflare']['email']
api_key = config['cloudflare']['api_key']

headers = {'Content-Type': content_type,
           'X-Auth-Key': api_key,
           'X-Auth-Email': email}

# Ipfy
ipfy_base_url = 'https://api.ipify.org/'


def check_response_errors(response):
    if response.status_code == 200 and response.headers.get('Content-Type', None) == 'application/json':
        return True

    logging.info(json.dumps({'time': str(datetime.datetime.now()),
                             'status': 'error',
                             'request_method': response.request.method,
                             'request_url': response.request.url,
                             'response_status_code': response.status_code,
                             'response_text': response.text}))
    exit(-1)


# get current ip
curr_ip_response = requests.get(url='{}?format=json'.format(ipfy_base_url, zone_name),
                                headers=headers)
check_response_errors(curr_ip_response)
curr_ip = curr_ip_response.json()['ip']

# get cloudflare zone id
zones_response = requests.get(url='{}zones/?name={}'.format(cf_base_url, zone_name),
                              headers=headers)
check_response_errors(zones_response)

zone_id = zones_response.json()['result'][0]['id']

# get cloudflare dns record id
dns_records_response = requests.get(
    url='{}zones/{}/dns_records?type=A&name={}'.format(cf_base_url, zone_id, dns_record_name),
    headers=headers)
check_response_errors(dns_records_response)

# check if ip is same
dns_record_id = dns_records_response.json()['result'][0]['id']

if dns_records_response.json()['result'][0]['content'] == curr_ip:
    logging.info(json.dumps({'time': str(datetime.datetime.now()),
                             'status': 'unchanged',
                             'ip': curr_ip,
                             'zone_name': zone_name,
                             'dns_record_name': dns_record_name}))
    exit(1)

# patch cloudflare dns record
patch_dns_record_response = requests.patch(url='{}zones/{}/dns_records/{}'.format(cf_base_url, zone_id, dns_record_id),
                                           data=json.dumps({'content': curr_ip}),
                                           headers=headers)

check_response_errors(patch_dns_record_response)

logging.warning(json.dumps({'time': str(datetime.datetime.now()),
                            'status': 'changed',
                            'ip': curr_ip,
                            'zone_name': zone_name,
                            'dns_record_name': dns_record_name}))
exit(1)
