# ddclient

Dynamic DNS client which updates Cloudflare A record with current IP.

## init project
```bash
virtualenv --python=<path/to/python3.6> venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

Create `config.ini` like this:
```ini
[cloudflare]
zone_name = YOUR_ZONE_NAME
dns_record_name = YOUR_DNS_RECORD_NAME
email = YOUR_EMAIL
api_key = YOUR_API_KEY
```

## run project
```bash
source venv/bin/activate
python ddclyent.py
```

## create a cron job
```bash
crontab -e
```
add line: `* * * * * cd /path/to/project/ && ./venv/bin/python ./ddclient.py`